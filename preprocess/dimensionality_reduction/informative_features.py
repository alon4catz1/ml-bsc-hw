import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

from preprocess.variables_encoding.one_hot import encode_data
from utils.data_fields import FIELDS


if __name__ == '__main__':
    df_dum, odors, classes = encode_data("../../data/mushrooms_data.txt", FIELDS)

    df = pd.concat([df_dum, odors, classes], axis=1)
    correlations = df.corr()[["class"]].abs().sort_values("class", ascending=False)
    sns.heatmap(correlations, square=True)
    plt.savefig(f"plots/informative_features")
    plt.show()
