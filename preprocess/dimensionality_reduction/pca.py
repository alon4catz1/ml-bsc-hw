import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
from pandas import DataFrame

from preprocess.variables_encoding.one_hot import encode_data
from utils.data_fields import FIELDS


if __name__ == '__main__':
    df_dum, odors, classes = encode_data("../../data/mushrooms_data.txt", FIELDS)

    pca = PCA()
    index = df_dum.index
    df_dum = DataFrame(pca.fit_transform(df_dum), index=index)
    df_dum = pd.concat([df_dum, odors], axis=1)
    plt.plot(np.cumsum(pca.explained_variance_ratio_))
    plt.title("PCA Variance")
    plt.savefig(f"plots/pca_variance")
    plt.show()

    ax = plt.axes(projection='3d')
    ax.scatter3D(df_dum[[0]], df_dum[[1]], df_dum[[2]], c=odors[["odor"]])
    plt.title("First 3 Components (Colored By Odor)")
    plt.show()
