import pandas as pd
from pandas import DataFrame
from sklearn.decomposition import PCA

EXTRACTING_FEATURES_BY = ["pca", "corr"]


def extract_features(df: DataFrame, features_count: int = 22, by: str = "pca", labels: DataFrame = None) -> DataFrame:
    if features_count > len(df.columns):
        raise Exception(f"'features_count' = {features_count} exceeds dataframe's columns amount")
    else:
        if by == "pca":
            pca = PCA(n_components=features_count)
            df = DataFrame(pca.fit_transform(df))
            return df
        elif by == "corr":
            if labels is None:
                raise Exception("by 'corr' requires labels for features correlations")
            else:
                concatenated_df = pd.concat([df, labels], axis=1)
                labels_column = labels.columns[0]
                correlation = concatenated_df.corr().abs()[[labels_column]].sort_values(labels_column, ascending=False)
                features = correlation.index[1:features_count + 1]
                return df[features]
        else:
            raise Exception(f"'by' = {by} is not a valid method")
