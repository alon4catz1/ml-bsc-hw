from typing import List

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from utils.data_fields import FIELDS
from utils.data_fields import COLUMNS

MISSING_DATA_FILLING_METHODS = ["frequency", "drop"]


def fill_missing_data(dataframe, mode):
    dataframe = dataframe.replace("-", np.nan)
    if mode == "frequency":
        for col in dataframe.columns:
            most_frequent_category = dataframe[col].mode()[0]
            dataframe.fillna(most_frequent_category, inplace=True)
    elif mode == "drop":
        dataframe.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)
    elif mode == "regression":
        pass  # Not implemented yet
    return dataframe


def encode_data(file_path: str, fields: List[str], fill_method: str = "frequency"):
    df = pd.read_csv(file_path, names=fields)
    df = fill_missing_data(df, fill_method)

    classes_column = df[["class"]]
    classes_values = pd.unique(classes_column.iloc[:, 0])
    for i in range(len(classes_values)):
        classes_column = classes_column.replace(classes_values[i], i)

    odors_column = df[["odor"]]
    odors_values = pd.unique(odors_column.iloc[:, 0])
    for i in range(len(odors_values)):
        odors_column = odors_column.replace(odors_values[i], i)

    df = df.drop(["class", "odor"], axis=1)
    df_dum = pd.get_dummies(df, drop_first=True)
    df_dum = df_dum.T.reindex(COLUMNS).T.fillna(0)

    return df_dum, odors_column, classes_column


if __name__ == '__main__':
    encoded_df, odors, classes = encode_data(f"../../data/mushrooms_data.txt", FIELDS)

    columns_amount = 9
    rows_amount = 8

    fig, ax = plt.subplots()
    ax.set_axis_off()
    table = ax.table(
        cellText=encoded_df.iloc[:rows_amount, :columns_amount].to_numpy(),
        colLabels=encoded_df.columns[:columns_amount],
        colColours=["palegreen"] * columns_amount,
        cellLoc='center',
        loc='best')
    table.auto_set_font_size(False)
    table.set_fontsize(10)

    ax.set_title("One-Hot Encoded Variables", fontweight="bold")

    plt.show()
