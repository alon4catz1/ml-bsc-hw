import matplotlib.pyplot as plt
import numpy as np

from sklearn import svm
from sklearn.decomposition import PCA

from preprocess.variables_encoding.one_hot import encode_data
from utils.data_fields import FIELDS


if __name__ == '__main__':
    df_dum, odors, classes = encode_data("../../../data/mushrooms_data.txt", FIELDS)

    pca = PCA(n_components=22)
    df_dum = pca.fit_transform(df_dum)

    X, Y = df_dum, classes.iloc[:, 0]

    X = X[:, :3]

    # make it binary classification problem
    X = X[np.logical_or(Y == 0, Y == 1)]
    Y = Y[np.logical_or(Y == 0, Y == 1)]

    model = svm.SVC(kernel='linear')
    clf = model.fit(X, Y)

    # The equation of the separating plane is given by all x so that np.dot(svc.coef_[0], x) + b = 0.
    # Solve for w3 (z)
    z = lambda x, y: (-clf.intercept_[0] - clf.coef_[0][0] * x - clf.coef_[0][1] * y) / clf.coef_[0][2]

    tmp = np.linspace(-2, 2, 30)
    x, y = np.meshgrid(tmp, tmp)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot3D(X[Y == 0, 0], X[Y == 0, 1], X[Y == 0, 2], 'ob')
    ax.plot3D(X[Y == 1, 0], X[Y == 1, 1], X[Y == 1, 2], 'sr')
    ax.plot_surface(x, y, z(x, y))
    ax.view_init(30, 60)
    plt.title("Linear SVM (3D)")
    plt.show()
