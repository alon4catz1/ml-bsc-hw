import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split

from preprocess.variables_encoding.one_hot import encode_data
from utils.data_fields import FIELDS
from utils.models_parameters import TEST_SIZE


if __name__ == '__main__':
    file_path = "../../../data/mushrooms_data_missing.txt"
    file_name = file_path.split("/")[-1].split(".")[0]
    df_dum, odors, classes = encode_data(file_path, FIELDS)

    pca = PCA(n_components=22)
    index = df_dum.index
    df_dum = pca.fit_transform(df_dum)

    X, y = df_dum[:, :2], classes.iloc[:, 0]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state=1, stratify=y)

    # figure number
    fignum = 1

    # fit the model
    for kernel in ('linear', 'poly', 'rbf'):
        clf = svm.SVC(kernel=kernel, gamma=2)
        clf.fit(X_train, y_train)

        # plot the line, the points, and the nearest vectors to the plane
        plt.figure(fignum, figsize=(20, 15))
        plt.clf()

        plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=80,
                    facecolors='none', zorder=10, edgecolors='k')
        plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, zorder=10, cmap=plt.cm.Paired,
                    edgecolors='k')

        plt.axis('tight')
        x_min = -3
        x_max = 3
        y_min = -3
        y_max = 3

        XX, YY = np.mgrid[x_min:x_max:200j, y_min:y_max:200j]
        Z = clf.decision_function(np.c_[XX.ravel(), YY.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(XX.shape)
        plt.figure(fignum, figsize=(20, 15))
        plt.pcolormesh(XX, YY, Z > 0, cmap=plt.cm.Paired)
        plt.contour(XX, YY, Z, colors=['k', 'k', 'k'], linestyles=['--', '-', '--'],
                    levels=[-.5, 0, .5])

        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)

        plt.xticks(())
        plt.yticks(())
        fignum = fignum + 1
        plt.savefig(f"plots/{file_name}_{kernel}")
        plt.show()
