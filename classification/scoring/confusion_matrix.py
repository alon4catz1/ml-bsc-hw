from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, accuracy_score

import matplotlib.pyplot as plt

from preprocess.dimensionality_reduction.features_extracting import EXTRACTING_FEATURES_BY, extract_features
from preprocess.variables_encoding.one_hot import encode_data, MISSING_DATA_FILLING_METHODS
from utils.data_fields import FIELDS
from utils.models_parameters import TEST_SIZE


if __name__ == '__main__':
    for fill_method in MISSING_DATA_FILLING_METHODS:
        for extraction_method in EXTRACTING_FEATURES_BY:
            file_path = "../../data/mushrooms_data_missing.txt"
            file_name = file_path.split("/")[-1].split(".")[0]
            df_dum, odors, classes = encode_data(file_path, FIELDS, fill_method=fill_method)

            X = extract_features(df_dum, 22, extraction_method, classes)
            y = classes.iloc[:, 0]

            #
            # Create training and test split
            #
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state=1, stratify=y)

            #
            # Standardize the data set
            #
            sc = StandardScaler()
            sc.fit(X_train)
            X_train_std = sc.transform(X_train)
            X_test_std = sc.transform(X_test)

            kernels = ["linear", "poly", "rbf"]
            for kernel in kernels:
                #
                # Fit the SVC model
                #
                svc = SVC(kernel=kernel, C=10.0, random_state=1)
                svc.fit(X_train, y_train)
                #
                # Get the predictions
                #
                y_pred = svc.predict(X_test)
                #
                # Calculate the confusion matrix
                #
                conf_matrix = confusion_matrix(y_true=y_test, y_pred=y_pred)
                #
                # Print the confusion matrix using Matplotlib
                #
                fig, ax = plt.subplots(figsize=(5, 5))
                ax.matshow(conf_matrix, cmap=plt.cm.Oranges, alpha=0.3)
                for i in range(conf_matrix.shape[0]):
                    for j in range(conf_matrix.shape[1]):
                        ax.text(x=j, y=i, s=conf_matrix[i, j], va='center', ha='center', size='xx-large')

                precision = precision_score(y_test, y_pred)
                recall = recall_score(y_test, y_pred)
                accuracy = accuracy_score(y_test, y_pred)

                scoring_text = f"Precision: {round(precision, 3)}\n" \
                               f"Recall: {round(recall, 3)}\n" \
                               f"Accuracy: {round(accuracy, 3)}"

                plt.xlabel('Predictions', fontsize=14)
                plt.ylabel('Actuals', fontsize=14)
                plt.title('Confusion Matrix', fontsize=16)
                plt.text(0.02, 0.5, scoring_text, fontsize=14, transform=plt.gcf().transFigure)
                plt.subplots_adjust(left=0.5)
                plt.savefig(f"plots/confusion_matrix/kernel/{kernel}/{file_name}_{fill_method}_{extraction_method}")
                plt.show()
