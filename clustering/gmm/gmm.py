import numpy as np
import matplotlib.pyplot as plt
from sklearn import mixture

import itertools

from scipy import linalg
import matplotlib as mpl
from sklearn.metrics import silhouette_score, homogeneity_score

from preprocess.dimensionality_reduction.features_extracting import EXTRACTING_FEATURES_BY, extract_features
from preprocess.variables_encoding.one_hot import encode_data, MISSING_DATA_FILLING_METHODS
from utils.data_fields import FIELDS
from utils.models_parameters import NUMBER_OF_CLUSTERS


def plot_results(x, y, means, covariances, title):
    splot = plt.subplot()
    for i, (mean, covar, color) in enumerate(zip(
            means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(y == i):
            continue
        plt.scatter(x.iloc[y == i, 0], x.iloc[y == i, 1], .8, c=color)

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)

    plt.xlim(min(x.iloc[:, 0]), max(x.iloc[:, 0]))
    plt.ylim(min(x.iloc[:, 1]), max(x.iloc[:, 1]))
    plt.xticks(())
    plt.yticks(())
    plt.title(title)


if __name__ == '__main__':
    for fill_method in MISSING_DATA_FILLING_METHODS:
        for extraction_method in EXTRACTING_FEATURES_BY:
            file_path = "../../data/mushrooms_data_missing.txt"
            file_name = file_path.split("/")[-1].split(".")[0]
            df_dum, odors, classes = encode_data(file_path, FIELDS, fill_method=fill_method)

            X = extract_features(df_dum, 22, extraction_method, classes)

            color_iter = itertools.cycle(
                ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f'])

            n_components = NUMBER_OF_CLUSTERS
            score = []
            aic = []
            bic = []
            class_homogeneity = []
            odor_homogeneity = []

            for n in n_components:
                gmm = mixture.GaussianMixture(n_components=n, max_iter=300, n_init=4)
                gmm.fit(X)
                labels = gmm.predict(X)
                if n > 1:
                    score.append(silhouette_score(X, labels))
                else:
                    score.append(0)
                aic.append(gmm.aic(X))
                bic.append(gmm.bic(X))
                class_homogeneity_avg = homogeneity_score(classes.iloc[:, 0], labels)
                print("For n_clusters =", n,
                      "The average class_homogeneity_score is :", class_homogeneity_avg)
                class_homogeneity.append(class_homogeneity_avg)

                odor_homogeneity_avg = homogeneity_score(odors.iloc[:, 0], labels)
                print("For n_clusters =", n,
                      "The average odor_homogeneity_score is :", odor_homogeneity_avg)
                odor_homogeneity.append(odor_homogeneity_avg)
                plot_results(X.iloc[:, :2], gmm.predict(X), gmm.means_, gmm.covariances_, f"GMM For {n} Clusters")
                plt.savefig(f"plots/{file_name}_{n}_{fill_method}_{extraction_method}")
                plt.show()

            plt.figure(figsize=(5, 5))
            plt.plot(n_components, score, label="silhouette")
            plt.plot(n_components, class_homogeneity, label="class homogeneity")
            plt.plot(n_components, odor_homogeneity, label="odor homogeneity")
            plt.legend(loc="upper left")
            plt.title("Scores")
            plt.savefig(f"plots/{file_name}_silhouette_and_homogeneity_{fill_method}_{extraction_method}")
            plt.show()

            plt.figure(figsize=(5, 5))
            plt.plot(n_components, aic, label="AIC")
            plt.plot(n_components, bic, label="BIC")
            plt.legend(loc="upper left")
            plt.title("Scores")
            plt.savefig(f"plots/{file_name}_aic_bic_{fill_method}_{extraction_method}")
            plt.show()
