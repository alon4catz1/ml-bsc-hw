import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, silhouette_samples, homogeneity_score

from preprocess.dimensionality_reduction.features_extracting import EXTRACTING_FEATURES_BY, extract_features
from preprocess.variables_encoding.one_hot import encode_data, MISSING_DATA_FILLING_METHODS
from utils.data_fields import FIELDS
from utils.models_parameters import NUMBER_OF_CLUSTERS


if __name__ == '__main__':
    for fill_method in MISSING_DATA_FILLING_METHODS:
        for extraction_method in EXTRACTING_FEATURES_BY:
            file_path = "../../data/mushrooms_data_missing.txt"
            file_name = file_path.split("/")[-1].split(".")[0]
            df_dum, odors, classes = encode_data(file_path, FIELDS, fill_method=fill_method)

            X = extract_features(df_dum, 22, extraction_method, classes)

            print(__doc__)

            range_n_clusters = NUMBER_OF_CLUSTERS
            silhouette = []
            class_homogeneity = []
            odor_homogeneity = []

            for n_clusters in range_n_clusters:
                # Create a subplot with 1 row and 2 columns
                fig, (ax1, ax2) = plt.subplots(1, 2)
                fig.set_size_inches(13, 5)

                # The 1st subplot is the silhouette plot
                # The silhouette coefficient can range from -1, 1 but in this example all
                # lie within [-0.1, 1]
                ax1.set_xlim([-0.1, 1])
                # The (n_clusters+1)*10 is for inserting blank space between silhouette
                # plots of individual clusters, to demarcate them clearly.
                ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

                # Initialize the clusterer with n_clusters value and a random generator
                # seed of 10 for reproducibility.
                clusterer = KMeans(n_clusters=n_clusters, n_init=5)
                cluster_labels = clusterer.fit_predict(X, y=odors)

                # The silhouette_score gives the average value for all the samples.
                # This gives a perspective into the density and separation of the formed
                # clusters
                if n_clusters > 1:
                    silhouette_avg = silhouette_score(X, cluster_labels)
                    print("For n_clusters =", n_clusters,
                          "The average silhouette_score is :", silhouette_avg)
                    silhouette.append(silhouette_avg)

                    class_homogeneity_avg = homogeneity_score(classes.iloc[:, 0], cluster_labels)
                    print("For n_clusters =", n_clusters,
                          "The average class_homogeneity_score is :", class_homogeneity_avg)
                    class_homogeneity.append(class_homogeneity_avg)

                    odor_homogeneity_avg = homogeneity_score(odors.iloc[:, 0], cluster_labels)
                    print("For n_clusters =", n_clusters,
                          "The average odor_homogeneity_score is :", odor_homogeneity_avg)
                    odor_homogeneity.append(odor_homogeneity_avg)

                # Compute the silhouette scores for each sample
                sample_silhouette_values = silhouette_samples(X, cluster_labels)

                y_lower = 10
                for i in range(n_clusters):
                    # Aggregate the silhouette scores for samples belonging to
                    # cluster i, and sort them
                    ith_cluster_silhouette_values = \
                        sample_silhouette_values[cluster_labels == i]

                    ith_cluster_silhouette_values.sort()

                    size_cluster_i = ith_cluster_silhouette_values.shape[0]
                    y_upper = y_lower + size_cluster_i

                    color = cm.nipy_spectral(float(i) / n_clusters)
                    ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                      0, ith_cluster_silhouette_values,
                                      facecolor=color, edgecolor=color, alpha=0.7)

                    # Label the silhouette plots with their cluster numbers at the middle
                    ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

                    # Compute the new y_lower for next plot
                    y_lower = y_upper + 10  # 10 for the 0 samples

                ax1.set_title("The silhouette plot for the various clusters.")
                ax1.set_xlabel("The silhouette coefficient values")
                ax1.set_ylabel("Cluster label")

                # The vertical line for average silhouette score of all the values
                ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

                ax1.set_yticks([])  # Clear the yaxis labels / ticks
                ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

                # 2nd Plot showing the actual clusters formed
                colors = cm.nipy_spectral(cluster_labels.astype(float) / n_clusters)
                ax2.scatter(X.iloc[:, 0], X.iloc[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                            c=colors, edgecolor='k')

                # Labeling the clusters
                centers = clusterer.cluster_centers_
                # Draw white circles at cluster centers
                ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                            c="white", alpha=1, s=200, edgecolor='k')

                for i, c in enumerate(centers):
                    ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                                s=50, edgecolor='k')

                ax2.set_title("The visualization of the clustered data.")
                ax2.set_xlabel("Feature space for the 1st feature")
                ax2.set_ylabel("Feature space for the 2nd feature")

                plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                              "with n_clusters = %d" % n_clusters),
                             fontsize=14, fontweight='bold')
                plt.savefig(f"plots/{file_name}_{n_clusters}_{fill_method}_{extraction_method}")
                plt.show()

            plt.figure(figsize=(5, 5))
            plt.plot(range_n_clusters, silhouette, label="silhouette")
            plt.plot(range_n_clusters, class_homogeneity, label="class homogeneity")
            plt.plot(range_n_clusters, odor_homogeneity, label="odor homogeneity")
            plt.legend(loc="upper left")
            plt.title("Scores")
            plt.savefig(f"plots/{file_name}_silhouette_and_homogeneity_{fill_method}_{extraction_method}")
            plt.show()
